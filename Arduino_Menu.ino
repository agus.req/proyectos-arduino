#include <Wire.h>
#include <LiquidCrystal_I2C.h>
#include <DHT.h>

#define DHTTYPE DHT11
int SensorTempPin = 2;

int pResistor = A0;
int LED = 4;

/////////////////////////////////////////////////

uint8_t outputPins[6] = {3, 5, 6, 9, 10, 11}; // PWM pins

/////////////////////////////////////////////////

int buttonENTER = 9;
int buttonL = 10;
int buttonU = 11;
int buttonD = 12;
int buttonR = 13;

////Menúes///////////////////////////////////////
int nPrinc = 0;
int nLCD = 1;
int nTemp = 2;
int nLDR = 3;
int nSens = 4;
int nHum = 5;
/////////////////////////////////////////////////

int buttonENTER_1= 0;
int buttonL_1= 0;
int buttonU_1= 0;
int buttonD_1= 0;
int buttonR_1= 0;
int Epres = 0; //Enter
int Lpres = 0; //Izquierda
int Upres = 0; //Arriba
int Dpres = 0; //Abajo
int Rpres = 0; //Derecha
int LDR = 0; //Fotoresistor

int acc = 0;
int prim = 0;
int nivel = 0;
int retrasomenu = 0;
int retrasolcd = 0;
int retardolcd = 1;
int primvezlcd = 0;
int retrasotemp = 0;
int retrasoled = 0;
int paso_datos = 0;
int paso_datosant = -1;
int paso_prim = 0;
int temp_lcd = 0;
int temp_lcdant = -1;
int hum_lcd = 0;
int hum_lcdant = -1;
int prueba_led = 0;
int prueba_ledant = -1;
int prueba_ledrgb = 0;
int prueba_ledrgbant = -1;
int largotexto = 0;
int largotextolim = 0;
int primveztexto = 0;

int valrgb=0;
int velrgb=1;

int menuposX = 1;
int menuposXant = -1;
int menuposY = 1;
int menuposYant = -1;

int maxXprinc = 2; //Menu Principal
int maxYprinc = 1;
int maxX1 = 1; //LCD
int maxY1 = 1;
int maxX2 = 2; //Temperatura
int maxY2 = 2;
int maxX3 = 2; //LDR
int maxY3 = 3;
int maxX4 = 1; //Panel Sensores
int maxY4 = 4;
int maxX5 = 2; //Humedad
int maxY5 = 2;


int iL = 0;
int jL = 0;
int MovSpeedLCD = 1; //Cantidad de desplazamiento de valores (Max 16)
int MovSpeedLCDant = 1;
int TimeSpeedLCD = 20; //Delay de 20ms. Delay tot=xMsec*20
int TimeSpeedLCDant = 20;

struct datos{
  int nivel;
  int acc;
};

byte puntos_suspensivos[8] = {
  B00000,
  B00000,
  B00000,
  B00000,
  B00000,
  B00000,
  B00000,
  B10101,
};

byte grados[8] = {
  B01100,
  B10010,
  B10010,
  B01100,
  B00000,
  B00000,
  B00000,
  B00000,
};

String texto="";
String texto2="";

DHT dht(SensorTempPin, DHTTYPE); //Temperatura

const int  en = 2, rw = 1, rs = 0, d4 = 4, d5 = 5, d6 = 6, d7 = 7, bl = 3;
const int i2c_addr = 0x3F;
LiquidCrystal_I2C lcd(i2c_addr, en, rw, rs, d4, d5, d6, d7, bl, POSITIVE);



void setup(){
   pinMode(buttonENTER, INPUT);
   pinMode(buttonL, INPUT);
   pinMode(buttonU, INPUT);
   pinMode(buttonD, INPUT);
   pinMode(buttonR, INPUT);
   pinMode(pResistor, INPUT);
   pinMode(LED, OUTPUT);

   lcd.begin(16,2);
   lcd.createChar(1, puntos_suspensivos);
   lcd.createChar(2, grados);
   delay(1000);
   lcd.clear();
   
   Serial.begin(9600);
   Serial.setTimeout(20);
}
         
void loop(){  
   int i,j,prim=1;
   float h = dht.readHumidity();
   float t = dht.readTemperature();
   int it=(int)t;
   int ih=(int)h;
   struct datos Data;
   buttonENTER_1 = digitalRead(buttonENTER); 
   buttonL_1 = digitalRead(buttonL); 
   buttonU_1 = digitalRead(buttonU); 
   buttonD_1 = digitalRead(buttonD); 
   buttonR_1 = digitalRead(buttonR); 
   LDR = analogRead(pResistor);

////////////////////////////////////////////////////////////
   
   if(buttonL_1 == HIGH){
    if (Lpres==0)
    {
      acc=1;
      Lpres=1;
    }
   }
   else{
    if (Lpres==1)
    {
      Lpres=0;
    }
   }

   
   if(buttonU_1 == HIGH){
    if (Upres==0)
    {
      acc=2;
      Upres=1;
    }
   }
   else{
    if (Upres==1)
    {
      Upres=0;
    }
   }

   
   if(buttonD_1 == HIGH){
    if (Dpres==0)
    {
      acc=3;
      Dpres=1;
    }
   }
   else{
    if (Dpres==1)
    {
      Dpres=0;
    }
   }
  
   
   if(buttonR_1 == HIGH){
    if (Rpres==0)
    {
      acc=4;
      Rpres=1;
    }
   }
   else{
    if (Rpres==1)
    {
      Rpres=0;
    }
   }

   if(buttonENTER_1 == HIGH){
    if (Epres==0)
    {
      acc=5;
      Epres=1;
    }
   }
   else{
    if (Epres==1)
    {
      Epres=0;
    }
   }

////////////////////////////////////////////////////////////
   
  if(acc!=0||prim==1)
  {
    (prim==1)?prim=0:prim;
    Data=menu(acc, nivel);

    acc=Data.acc;
    nivel=Data.nivel;
  }
  
  if(paso_datos==1)
  {
    if(nivel==nLCD)
    {
      String texto = Serial.readString();
      if(texto!="")
      {
        texto.remove(texto.length()-1);
        texto2=texto;
        Serial.print("Se ha impreso \"");
        Serial.print(texto2);
        Serial.print("\" en el LCD\n");
        if (texto.length() > 0 && texto.length() <= 16)
        {
          lcd.setCursor(0,1);
          lcd.print("                ");
          lcd.setCursor(0,1);
          lcd.print(texto2);
        }
        else
        {
          if(MovSpeedLCD<=16)
          {
            iL=0;
            largotexto=texto2.length();
            if(texto2.length()%MovSpeedLCD != 0)
              for(i=0;i<(16-largotexto%MovSpeedLCD);i++)
                texto2 += " ";
            largotextolim=largotexto-16;
            retrasolcd=1;
            retardolcd=0;
            primvezlcd=50;
          }
          else
          {
            lcd.setCursor(0,1);
            lcd.print("                ");
            lcd.setCursor(0,1);
            lcd.print("ERROR  (Monitor)");
            Serial.print("El valor de MovSpeedLCD no puede ser mayor a 16, generaria fallos al saltar de posicion en posicion y habia parte de texto que no seria mostrada. Modifiquelo y compile nuevamente");
          }
        }
        texto="";
      }
      if(retrasolcd==1)
      {
        if(retardolcd==0)
        {
          if(iL<=largotexto && largotextolim>-MovSpeedLCD)
          {
            lcd.setCursor(0,1);
            lcd.print("                ");
            lcd.setCursor(0,1);
            for(jL=0;jL<16;jL++)
              if(largotextolim>-MovSpeedLCD)
                lcd.print(texto2[(MovSpeedLCD*iL)+jL]);
            largotextolim=largotextolim - MovSpeedLCD;
            iL++;
          }
          else
            retrasolcd=0;
        }
        if(primvezlcd==50)
        {
          primvezlcd--;
          retardolcd++;
        }
        if(primvezlcd<50)
          primvezlcd--;
        if(primvezlcd<=0)
          retardolcd++;
        if(retardolcd==TimeSpeedLCD)
          retardolcd=0;
      }
    }
    if(nivel==nTemp)
    {
      if(retrasomenu==0)
      {
        if(paso_prim==1)
        {
          Serial.print("Se muestran los valores de temperatura\n");
          paso_prim=0;
        }
        Serial.print(t);
        Serial.print("\n");
      }
      retrasomenu++;
      delay(20);
      if(retrasomenu==50)
         retrasomenu=0;
    }
    if(nivel==nHum)
    {
      if(retrasomenu==0)
      {
        if(paso_prim==1)
        {
          Serial.print("Se muestran los valores de humedad\n");
          paso_prim=0;
        }
        Serial.print(h);
        Serial.print("\n");
      }
      retrasomenu++;
      delay(20);
      if(retrasomenu==50)
         retrasomenu=0;
    }
     if(nivel==nLDR)
    {
      if(retrasomenu==0)
      {
        if(paso_prim==1)
        {
          Serial.print("Se muestran los Voltajes del Fotoresistor\n");
          paso_prim=0;
        }
        Serial.print(LDR*0.005);
        Serial.print("V\n");
      }
      retrasomenu++;
      delay(20);
      if(retrasomenu==50)
         retrasomenu=0;
    }

  }
  else
    delay(20);

////////////////////////////////////////////////////////////

  if(temp_lcd==1)
    {
      if(retrasotemp==0)
      {
        lcd.setCursor(0,0);
        lcd.print("Menu Temp   ");lcd.print(it);lcd.write(2);lcd.print("C");
      }
      retrasotemp++;
      if(retrasotemp==5)
         retrasotemp=0;
    }
    
  if(hum_lcd==1)
    {
      if(retrasotemp==0)
      {
        lcd.setCursor(0,0);
        lcd.print("Menu Hum    ");lcd.print(ih);lcd.print("%");
      }
      retrasotemp++;
      if(retrasotemp==5)
         retrasotemp=0;
    }
    
  if(prueba_led==1)
  {

    if(retrasoled==0)
    {
      if(LDR>450)
      {
        digitalWrite(LED, LOW);
      }
      else
      {
        digitalWrite(LED, HIGH);
      }
    }
    retrasoled++;
    if(retrasoled==25)
      retrasoled=0;
  }
  if(prueba_ledrgb==1)
  {
    lucesitas3(0,valrgb);
   //trueHSV(0, valrgb);
   //powerHSV(0, valrgb);
   //sineLED(0, valrgb);
   if(LDR>0 && LDR<=300)
   {
    velrgb=10;
   }
   if(LDR>300 && LDR<=450)
   {
    velrgb=6;
   }
   if(LDR>450 && LDR<=600)
   {
    velrgb=3;
   }
   if(LDR>600)
   {
    velrgb=1;
   }
   
   valrgb=valrgb+velrgb;
   if(valrgb>=540) 
    valrgb=0;
  }

////////////////////////////////////////////////////////////

}



struct datos menu(int acc, int nivel)
{
  struct datos devolver;
  if(acc==1)
  {
    if(nivel==nPrinc||nivel==nLCD||nivel==nTemp||nivel==nLDR||nivel==nSens||nivel==nHum)
      if(menuposX>1)
        menuposX--;
  }

  else if(acc==2)
  {
    if(nivel==nPrinc||nivel==nLCD||nivel==nTemp||nivel==nLDR||nivel==nSens||nivel==nHum)
      if(menuposY>1)
        menuposY--;
  }

  else if(acc==3)
  {
    if(nivel==nPrinc)
      if(menuposY<maxYprinc)
        menuposY++;
    if(nivel==nLCD)
      if(menuposY<maxY1)
        menuposY++;
    if(nivel==nTemp)
      if(menuposY<maxY2)
        menuposY++;
   if(nivel==nLDR)
      if(menuposY<maxY3)
        menuposY++;
   if(nivel==nSens)
      if(menuposY<maxY4)
        menuposY++;
   if(nivel==nHum)
      if(menuposY<maxY5)
        menuposY++;
    
  }

  else if(acc==4)
  {
    if(nivel==nPrinc)
      if(menuposX<maxXprinc)
        menuposX++;
    if(nivel==nLCD)
      if(menuposX<maxX1)
        menuposX++;
    if(nivel==nTemp)
      if(menuposX<maxX2)
        menuposX++;
    if(nivel==nLDR)
      if(menuposX<maxX3)
        menuposX++;
    if(nivel==nSens)
      if(menuposX<maxX4)
        menuposX++;
    if(nivel==nHum)
      if(menuposX<maxX5)
        menuposX++;
  }

  else if(acc==5)
  {
    if(nivel==nPrinc)
    {
      if(menuposX==1)
      {
        nivel=nLCD;
        menuposX=1;
        menuposXant=-1;
        menuposY=1;
        menuposYant=-1;
        paso_datos=1;
        primveztexto=1;
      }
      else if(menuposX==2)
      {
        nivel=nSens;
        menuposX=1;
        menuposXant=-1;
        menuposY=1;
        menuposYant=-1;
      }
    }
    else if(nivel==nLCD)
    {
      if(menuposX==1)
      {
        nivel=nPrinc;
        menuposX=1;
        menuposXant=-1;
        menuposY=1;
        menuposYant=-1;
        paso_datos=0;
        retrasolcd=0;
        retardolcd=0;
        texto2="";
      }
    }
    else if(nivel==nTemp)
    {
      if(menuposY==1)
      {
        if(menuposX==1)
        {
          nivel=nSens;
          menuposX=1;
          menuposXant=-1;
          menuposY=1;
          menuposYant=-1;
          paso_datos=0;
          temp_lcd=0;
        } 
        if(menuposX==2)
        {
          if(paso_datos==0)
          {
            paso_datos=1;
            paso_prim=1;
          }
          else
          {
            paso_datos=0;
          }
        }
      }
      else if(menuposY==2)
      {
        if(temp_lcd==0)
          temp_lcd=1;
        else
          temp_lcd=0;
      }
    }
    else if(nivel==nHum)
    {
      if(menuposY==1)
      {
        if(menuposX==1)
        {
          nivel=nSens;
          menuposX=1;
          menuposXant=-1;
          menuposY=2;
          menuposYant=-1;
          paso_datos=0;
          hum_lcd=0;
        } 
        if(menuposX==2)
        {
          if(paso_datos==0)
          {
            paso_datos=1;
            paso_prim=1;
          }
          else
          {
            paso_datos=0;
          }
        }
      }
      else if(menuposY==2)
      {
        if(hum_lcd==0)
        {
          hum_lcd=1;
        }
        else
        {
          hum_lcd=0;
        }
      }
    }
    else if(nivel==nLDR)
    {
      if(menuposY==1)
      {
        if(menuposX==1)
        {
          nivel=nSens;
          menuposX=1;
          menuposXant=-1;
          menuposY=3;
          menuposYant=-1;
          paso_datos=0;
          prueba_led=0;
          prueba_ledrgb=0;
          digitalWrite(LED, LOW);
          analogWrite(3, 0);
          analogWrite(5, 0);
          analogWrite(6, 0);
        }
        if(menuposX==2)
        {
          if(paso_datos==0)
            {
              paso_datos=1;
              paso_prim=1;
            }
            else
            {
              paso_datos=0;
            }
        }
      }
      else if(menuposY==2)
      {
        if(prueba_led==0)
          prueba_led=1;
        else
        {
          prueba_led=0;
          digitalWrite(LED, LOW);
        }
      }
      else if(menuposY==3)
      {
        if(prueba_ledrgb==0)
          prueba_ledrgb=1;
        else
        {
          prueba_ledrgb=0;
          analogWrite(3, 0);
          analogWrite(5, 0);
          analogWrite(6, 0);
        }
      }
    }
    else if(nivel==nSens)
    {
      if(menuposY==1)
      {
        nivel=nTemp;
        menuposX=1;
        menuposXant=-1;
        menuposY=1;
        menuposYant=-1;
      }
      if(menuposY==2)
      {
        nivel=nHum;
        menuposX=1;
        menuposXant=-1;
        menuposY=1;
        menuposYant=-1;
      }
      if(menuposY==3)
      {
        nivel=nLDR;
        menuposX=1;
        menuposXant=-1;
        menuposY=1;
        menuposYant=-1;
      }
      if(menuposY==4)
      {
        nivel=nPrinc;
        menuposX=1;
        menuposXant=-1;
        menuposY=1;
        menuposYant=-1;
      }
    }
  }
  
  if(menuposX!=menuposXant || menuposY!=menuposYant || paso_datos!=paso_datosant || temp_lcd!=temp_lcdant || prueba_led!=prueba_ledant || hum_lcd!=hum_lcdant || prueba_ledrgb!=prueba_ledrgbant )
  {
    lcd.clear();
    if(nivel==nPrinc)
    {
      lcd.setCursor(0,0);
      lcd.print("Menu Principal");
      lcd.setCursor(0,1);
      if(menuposX==1)
        lcd.print(">LCD  Sensores");
      else if(menuposX==2)
        lcd.print(" LCD >Sensores");
    }
    else if(nivel==nLCD)
    {
      lcd.setCursor(0,0);
      lcd.print("Menu LCD >Volver");
      lcd.setCursor(0,1);
      if(primveztexto==1)
      {
        lcd.print("Esperando texto"); lcd.write(1);
        primveztexto=0;
      }
    }
    else if(nivel==nTemp)
    {
      lcd.setCursor(0,0);
      if(temp_lcd==0)
        lcd.print("Menu Temp (");lcd.print(menuposY);lcd.print(")");
      lcd.setCursor(0,1);
      if(menuposY==1)
      {
        if(menuposX==1)
        {
          if(paso_datos==0)
            lcd.print(">Volver  Enviar");
          else
            lcd.print(">Volver  Detener");
        }
        if(menuposX==2)
        {
          if(paso_datos==0)
          {
            lcd.print(" Volver >Enviar");
          }
          else
          {
            lcd.print(" Volver >Detener");
          }
        }
      }
      else if(menuposY==2)
      {
        if(temp_lcd==0)
          lcd.print(">En Pantalla OFF");
        else
          lcd.print(">En Pantalla ON");
      }
    }
    else if(nivel==nHum)
    {
      lcd.setCursor(0,0);
      if(hum_lcd==0)
        lcd.print("Menu Hum (");lcd.print(menuposY);lcd.print(")");
      lcd.setCursor(0,1);
      if(menuposY==1)
      {
        if(menuposX==1)
        {
          if(paso_datos==0)
            lcd.print(">Volver  Enviar");
          else
            lcd.print(">Volver  Detener");
        }
        if(menuposX==2)
        {
          if(paso_datos==0)
          {
            lcd.print(" Volver >Enviar");
          }
          else
          {
            lcd.print(" Volver >Detener");
          }
        }
      }
      else if(menuposY==2)
      {
        if(hum_lcd==0)
          lcd.print(">En Pantalla OFF");
        else
          lcd.print(">En Pantalla ON");
      }
    }
    else if(nivel==nLDR)
    {
      lcd.setCursor(0,0);
      lcd.print("Menu LDR (");lcd.print(menuposY);lcd.print(")");
      lcd.setCursor(0,1);
      if(menuposY==1)
      {
        if(menuposY==1)
        {
          if(menuposX==1)
          {
            if(paso_datos==0)
              lcd.print(">Volver  Enviar");
            else
              lcd.print(">Volver  Detener");
          }
        }
        if(menuposX==2)
        {
          if(paso_datos==0)
          {
            lcd.print(" Volver >Enviar");
          }
          else
          {
            lcd.print(" Volver >Detener");
          }
        }
      }
      else if(menuposY==2)
      {
        if(prueba_led==0)
          lcd.print(">Prueba LED OFF");
        else
          lcd.print(">Prueba LED ON");
      }
      else if(menuposY==3)
      {
        if(prueba_ledrgb==0)
          lcd.print(">Prueba RGB OFF");
        else
          lcd.print(">Prueba RGB ON");
      }
    }
    else if(nivel==nSens)
    {
      lcd.setCursor(0,0);
      lcd.print("Menu Sensores(");lcd.print(menuposY);lcd.print(")");
      lcd.setCursor(0,1);
      if(menuposY==1)
      {
        lcd.print("> Temperatura");
      }
      if(menuposY==2)
      {
        lcd.print("> Humedad");
      }
      if(menuposY==3)
      {
        lcd.print("> Fotoresistor");
      }
      if(menuposY==4)
      {
        lcd.print("> Volver");
      }
    }
    menuposXant=menuposX;
    menuposYant=menuposY;
    paso_datosant=paso_datos;
    temp_lcdant=temp_lcd;
    prueba_ledant=prueba_led;
    hum_lcdant=hum_lcd;
    prueba_ledrgbant=prueba_ledrgb;
  }
  devolver.nivel=nivel;
  devolver.acc=0;
  return devolver;
}

void lucesitas3(byte LED, int angle)
{
  int red, green, blue;

  if(angle<180)
  {
    red=255*pow(cos((angle/(360/PI))),2);
    green=255*pow(cos((angle/(360/PI))-(PI/2)),2);
    blue=0;
  }
  if(angle>180 && angle<=360)
  {
    blue=255*pow(cos((angle/(360/PI))),2);
    green=255*pow(cos((angle/(360/PI))-(PI/2)),2);
    red=0;
  }
  if(angle>360 && angle<=540)
  {
    blue=255*pow(cos((angle/(360/PI))),2);
    red=255*pow(cos((angle/(360/PI))-(PI/2)),2);
    green=0;
  }
                
  setRGBpoint2(LED, red, green, blue);
}

void setRGBpoint2(byte LED, int red, int green, int blue)
{
  analogWrite(outputPins[LED*3], red);
  analogWrite(outputPins[LED*3+1], green);
  analogWrite(outputPins[LED*3+2], blue);
}
